﻿using System.Configuration;

namespace WebApplication5.Adapter
{
    public class CommonBase
    {
        private readonly string KeyConnString = "BasicContext";
        protected string connString;
        public CommonBase()
        {
            connString = ConfigurationManager.ConnectionStrings[KeyConnString].ConnectionString;
        }
    }
}
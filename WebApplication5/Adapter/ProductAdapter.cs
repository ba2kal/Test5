﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using log4net;

namespace WebApplication5.Adapter
{
    public class ProductAdapter : CommonBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<ProductModel> ProductList()
        {
            var result = new List<ProductModel>();
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("SELECT*FROM MstProductInformation", conn))
            {
                conn.Open();

                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(new ProductModel
                        {
                            ProductId = (int)reader["ProductId"],
                            ProductName = reader["ProductName"].ToString(),
                            ProductPrice = (int)reader["ProductPrice"],
                            ProductMemo = reader["ProductMemo"].ToString(),
                            ProductSafeDay = (int)reader["ProductSafeDay"],
                            RegisterDate = (DateTime)reader["RegisterDate"],
                            ModifyDate = (DateTime)reader["ModifyDate"],
                        });
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public List<ProductModel> SearchProductByName(string name)
        {
            var result = new List<ProductModel>();
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("SELECT*FROM MstProductInformation WHERE ProductName LIKE '%" + @name + "%'", conn))
            {
                conn.Open();
                SqlParameter getName = new SqlParameter("@name", SqlDbType.NVarChar);
                getName.Value = name;
                cmd.Parameters.Add(getName);

                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new ProductModel
                        {
                            ProductId = (int)reader["ProductId"],
                            ProductName = reader["ProductName"].ToString(),
                            ProductPrice = (int)reader["ProductPrice"],
                            ProductMemo = reader["ProductMemo"].ToString(),
                            ProductSafeDay = (int)reader["ProductSafeDay"],
                            RegisterDate = (DateTime)reader["RegisterDate"],
                            ModifyDate = (DateTime)reader["ModifyDate"],
                        });
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public List<ProductModel> SearchProductById(int prId)
        {
            var result = new List<ProductModel>();
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("SELECT*FROM MstProductInformation WHERE ProductId = @prId", conn))
            {
                conn.Open();
                SqlParameter getId = new SqlParameter("@prId", SqlDbType.Int);
                getId.Value = prId;
                cmd.Parameters.Add(getId);

                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new ProductModel
                        {
                            ProductId = (int)reader["ProductId"],
                            ProductName = reader["ProductName"].ToString(),
                            ProductPrice = (int)reader["ProductPrice"],
                            ProductMemo = reader["ProductMemo"].ToString(),
                            ProductSafeDay = (int)reader["ProductSafeDay"],
                            RegisterDate = (DateTime)reader["RegisterDate"],
                            ModifyDate = (DateTime)reader["ModifyDate"],
                        });
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public bool AddProductSubmit(ProductModel model)
        {
            var result = false;
            string sql = "INSERT INTO MstProductInformation VALUES (@name, @price, @memo, @safeday, @registerdate, @modifydate)";
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlParameter paramname = new SqlParameter("@name", SqlDbType.NVarChar);
                paramname.Value = model.ProductName;
                cmd.Parameters.Add(paramname);
                SqlParameter paramprice = new SqlParameter("@price", SqlDbType.Int);
                paramprice.Value = model.ProductPrice;
                cmd.Parameters.Add(paramprice);
                SqlParameter parammemo = new SqlParameter("@memo", SqlDbType.NVarChar);
                parammemo.Value = model.ProductMemo;
                cmd.Parameters.Add(parammemo);
                SqlParameter paramsafedate = new SqlParameter("@safeday", SqlDbType.Int);
                paramsafedate.Value = model.ProductSafeDay;
                cmd.Parameters.Add(paramsafedate);
                SqlParameter paramresisterdate = new SqlParameter("@registerdate", SqlDbType.DateTime);
                paramresisterdate.Value = model.RegisterDate;
                cmd.Parameters.Add(paramresisterdate);
                SqlParameter parammodifydate = new SqlParameter("@modifydate", SqlDbType.DateTime);
                parammodifydate.Value = model.ModifyDate;
                cmd.Parameters.Add(parammodifydate);

                try
                {
                    cmd.ExecuteNonQuery();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public ProductModel ModifyProductForm(int? productId)
        {
            var result = new ProductModel();
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("SELECT*FROM MstProductInformation WHERE ProductId = " + productId, conn))
            {
                conn.Open();
                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.ProductId = (int)reader["ProductId"];
                        result.ProductName = reader["ProductName"].ToString();
                        result.ProductPrice = (int)reader["ProductPrice"];
                        result.ProductMemo = reader["ProductMemo"].ToString();
                        result.ProductSafeDay = (int)reader["ProductSafeDay"];
                        result.RegisterDate = (DateTime)reader["RegisterDate"];
                        result.ModifyDate = (DateTime)reader["ModifyDate"];
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public bool ModifyProductSubmit(ProductModel model)
        {
            var result = false;
            string sql = "UPDATE MstProductInformation SET ProductName = @name, ProductPrice = @price, ProductMemo = @memo, ProductSafeDay = @safeday, RegisterDate = @registerdate, ModifyDate = @modifydate WHERE ProductId= @prId";
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();

                SqlParameter paramname = new SqlParameter("@name", SqlDbType.NVarChar);
                paramname.Value = model.ProductName;
                cmd.Parameters.Add(paramname);
                SqlParameter paramprice = new SqlParameter("@price", SqlDbType.Int);
                paramprice.Value = model.ProductPrice;
                cmd.Parameters.Add(paramprice);
                SqlParameter parammemo = new SqlParameter("@memo", SqlDbType.NVarChar);
                parammemo.Value = model.ProductMemo;
                cmd.Parameters.Add(parammemo);
                SqlParameter paramsafedate = new SqlParameter("@safeday", SqlDbType.Int);
                paramsafedate.Value = model.ProductSafeDay;
                cmd.Parameters.Add(paramsafedate);
                SqlParameter paramresisterdate = new SqlParameter("@registerdate", SqlDbType.DateTime);
                paramresisterdate.Value = model.RegisterDate;
                cmd.Parameters.Add(paramresisterdate);
                SqlParameter parammodifydate = new SqlParameter("@modifydate", SqlDbType.DateTime);
                parammodifydate.Value = model.ModifyDate;
                cmd.Parameters.Add(parammodifydate);
                SqlParameter prId = new SqlParameter("@prId", SqlDbType.Int);
                prId.Value = model.ProductId;
                cmd.Parameters.Add(prId);

                try
                {
                    cmd.ExecuteNonQuery();
                    result = true;
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public List<InventoryModel> SearchInventory(int? param)
        {
            if(param == null)
            {
                param = 0;
            }
            var result = new List<InventoryModel>();
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("SELECT*FROM TrnInventory WHERE ProductId = @param", conn))
            {
                conn.Open();
                SqlParameter getParam = new SqlParameter("@param", SqlDbType.Int);
                getParam.Value = param;
                cmd.Parameters.Add(getParam);

                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new InventoryModel
                        {
                            InventoryId = (int)reader["InventoryId"],
                            ProductId = (int)reader["ProductId"],
                            InventoryName = reader["InventoryName"].ToString(),
                            InventoryPrice = (int)reader["InventoryPrice"],
                            InventoryCookDate = (DateTime)reader["InventoryCookDate"],
                            InventorySafeDate = (DateTime)reader["InventorySafeDate"],
                            ProductSafeDay = (int)reader["ProductSafeDay"],
                            Amount = (int)reader["Amount"]
                        });
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public int DeleteProduct(int? prId)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("DELETE FROM MstProductInformation WHERE ProductId = @prId", conn))
            {
                conn.Open();
                SqlParameter paramPrId = new SqlParameter("@prId", SqlDbType.Int);
                paramPrId.Value = prId;
                cmd.Parameters.Add(paramPrId);
                try
                {
                    cmd.ExecuteNonQuery();
                    result = 4;
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public ProductModel DetailProductForm(int prId)
        {
            var result = new ProductModel();

            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand("SELECT*FROM MstProductInformation WHERE ProductId = @prId", conn))
            {
                conn.Open();
                SqlParameter paramPrId = new SqlParameter("@prId", SqlDbType.Int);
                paramPrId.Value = prId;
                cmd.Parameters.Add(paramPrId);
                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        result.ProductId = (int)reader["ProductId"];
                        result.ProductName = reader["ProductName"].ToString();
                        result.ProductPrice = (int)reader["ProductPrice"];
                        result.ProductMemo = reader["ProductMemo"].ToString();
                        result.ProductSafeDay = (int)reader["ProductSafeDay"];
                        result.RegisterDate = (DateTime)reader["RegisterDate"];
                        result.ModifyDate = (DateTime)reader["ModifyDate"];
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }
    }
}
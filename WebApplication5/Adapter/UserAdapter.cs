﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models.User;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using log4net;

namespace WebApplication5.Adapter
{
    public class UserAdapter : CommonBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool AddUserSubmit(UserModel model)
        {
            var result = false;
            string sql = "INSERT INTO MstUserInformation (UserId, UserPass, UserLastName, UserFirstName, UserSex, UserBirthday, UserEmail, UserPhone, UserAddress, RegisterDate) VALUES (@userId, @userPass, @userLastName, @userFirstName, @userSex, @userBirthday, @userEmail, @userPhone, @userAddress, @registerDate)";
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlParameter userId = new SqlParameter("@userId", SqlDbType.VarChar);
                userId.Value = model.UserId;
                cmd.Parameters.Add(userId);
                SqlParameter userPass = new SqlParameter("@userPass", SqlDbType.VarChar);
                userPass.Value = model.UserPass;
                cmd.Parameters.Add(userPass);
                SqlParameter userLastName = new SqlParameter("@userLastName", SqlDbType.NVarChar);
                userLastName.Value = model.UserLastName;
                cmd.Parameters.Add(userLastName);
                SqlParameter userFirstName = new SqlParameter("@userFirstName", SqlDbType.NVarChar);
                userFirstName.Value = model.UserFirstName;
                cmd.Parameters.Add(userFirstName);
                SqlParameter userSex = new SqlParameter("@userSex", SqlDbType.Int);
                userSex.Value = model.UserSex;
                cmd.Parameters.Add(userSex);
                SqlParameter userBirthday = new SqlParameter("@userBirthday", SqlDbType.DateTime);
                userBirthday.Value = model.UserBirthday;
                cmd.Parameters.Add(userBirthday);
                SqlParameter userEmail = new SqlParameter("@userEmail", SqlDbType.VarChar);
                userEmail.Value = model.UserEmail;
                cmd.Parameters.Add(userEmail);
                SqlParameter userPhone = new SqlParameter("@userPhone", SqlDbType.VarChar);
                userPhone.Value = model.UserPhone;
                cmd.Parameters.Add(userPhone);
                SqlParameter userAddress = new SqlParameter("@userAddress", SqlDbType.NVarChar);
                userAddress.Value = model.UserAddress;
                cmd.Parameters.Add(userAddress);
                SqlParameter registerDate = new SqlParameter("@registerDate", SqlDbType.DateTime);
                registerDate.Value = DateTime.Now;
                cmd.Parameters.Add(registerDate);

                try
                {
                    cmd.ExecuteNonQuery();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                    Console.WriteLine("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }

            }
            return result;
        }

        public UserModel SignInSubmit(string id, string pass)
        {
            UserModel result = new UserModel();
            string sql = "SELECT * FROM MstUserInformation WHERE UserId = @userId AND UserPass = @userPass";
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();

                SqlParameter userId = new SqlParameter("@userId", SqlDbType.VarChar);
                userId.Value = id;
                cmd.Parameters.Add(userId);
                SqlParameter userPass = new SqlParameter("@userPass", SqlDbType.VarChar);
                userPass.Value = pass;
                cmd.Parameters.Add(userPass);

                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.UserId = reader["UserId"].ToString();
                        result.UserLastName = reader["UserLastName"].ToString();
                        result.UserFirstName = reader["UserFirstName"].ToString();
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public UserModel MyPageForm(string id)
        {
            UserModel result = new UserModel();
            string sql = "SELECT*FROM MstUserInformation WHERE UserId = @userId";
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();

                SqlParameter userId = new SqlParameter("@userId", SqlDbType.VarChar);
                userId.Value = id;
                cmd.Parameters.Add(userId);
                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.UserNumber = (int)reader["UserNumber"];
                        result.UserId = reader["UserId"].ToString();
                        result.UserPass = reader["UserPass"].ToString();
                        result.UserLastName = reader["UserLastName"].ToString();
                        result.UserFirstName = reader["UserFirstName"].ToString();
                        result.UserSex = (int)reader["UserSex"];
                        result.UserBirthday = (DateTime)reader["UserBirthday"];
                        result.UserEmail = reader["UserEmail"].ToString();
                        result.UserPhone = reader["UserPhone"].ToString();
                        result.UserAddress = reader["UserAddress"].ToString();
                        result.RegisterDate = (DateTime)reader["RegisterDate"];
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
                return result;
            }
        }

        public bool CheckUserId(string inputId)
        {
            bool result = false;
            string sql = "SELECT*FROM MstUserInformation WHERE UserId = @userId";
            using (SqlConnection conn = new SqlConnection(this.connString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();

                SqlParameter userId = new SqlParameter("@userId", SqlDbType.VarChar);
                userId.Value = inputId;
                cmd.Parameters.Add(userId);
                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        result = true;
                    }
                }
                catch (Exception e)
                {
                    log.Debug("error=" + e.Message);
                }
                finally
                {
                    conn.Close();
                }
                return result;
            }
        }
    }
}
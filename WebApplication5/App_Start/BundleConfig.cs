﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication5
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/Scripts/jquery-ui*"));
            bundles.Add(new StyleBundle("~/bundles/jqueryui/css").Include(
                       "~/Content/themes/base/jquery-ui.css",
                       "~/Content/themes/base/jquery*"
                       ));

            // 開発と学習には、Modernizr の開発バージョンを使用します。次に、実稼働の準備が
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/business-casual.css"));

            bundles.Add(new ScriptBundle("~/user/register/js").Include(
                "~/Scripts/User/register.js"));
            bundles.Add(new ScriptBundle("~/user/sign/js").Include(
                "~/Scripts/User/sign.js"));

            bundles.Add(new ScriptBundle("~/product/product/js").Include(
                        "~/Scripts/Product/product.js"));
            bundles.Add(new ScriptBundle("~/product/inventory/js").Include(
                       "~/Scripts/Product/inventory.js"));
            bundles.Add(new StyleBundle("~/product/productList/css").Include(
                       "~/Content/Product/productList.css"));
            bundles.Add(new StyleBundle("~/product/inventory/css").Include(
                       "~/Content/Product/inventory.css"));

            bundles.Add(new ScriptBundle("~/bundles/home/js").Include(
                      "~/Scripts/Home/bootstrap.js", 
                      "~/Scripts/Home/bootstrap.min.js", 
                      "~/Scripts/Home/jquery.js"));

            
            bundles.Add(new StyleBundle("~/Mobiscroll/css").Include(
                       "~/Content/Mobiscroll/mobiscroll*"
                       ));
            bundles.Add(new ScriptBundle("~/Mobiscroll/js").Include(
                        "~/Scripts/Mobiscroll/mobiscroll*"
                        ));

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApplication5.Adapter;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{

    public class ProductController : Controller
    {
        private ProductModel mstModel = new ProductModel();
        private InventoryModel trnModel = new InventoryModel();
        private List<ProductModel> productList = new List<ProductModel>();
        private List<InventoryModel> inventoryList = new List<InventoryModel>();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductList()
        {
            var adapter = new ProductAdapter();
            var model = adapter.ProductList();
            return View(model);
        }

        public ActionResult DetailProductForm(int prId)
        {
            var adapter = new ProductAdapter();
            var model = adapter.DetailProductForm(prId);
            return View(model);
        }

        public ActionResult SearchProductTable(string mode, string param)
        {
            var adapter = new ProductAdapter();
            if (mode == "string")
            {
                productList = adapter.SearchProductByName(param);
            }
            if (mode == "int")
            {
                int prId = Convert.ToInt32(param);
                productList = adapter.SearchProductById(prId);
            }
            return View(productList);
        }

        public ActionResult InventoryViewTable(int? prId)
        {
            if (prId == null)
            {
                return Redirect("~/Home");
            }
            else
            {
                var adapter = new ProductAdapter();
                inventoryList = adapter.SearchInventory(prId);
                return View(inventoryList);
            }
        }

        public ActionResult AddProductForm()
        {
            return View();
        }

        public ActionResult ModifyProductForm(int prId)
        {
            var adapter = new ProductAdapter();
            mstModel = adapter.ModifyProductForm(prId);
            return View(mstModel);
        }

        [HttpPost]
        public JsonResult DeleteProduct(int? prId)
        {
            int result = 0;
            var adapter = new ProductAdapter();
            if (prId != null)
            {
                result = adapter.DeleteProduct(prId);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult AddProductSubmit(ProductModel model)
        {
            var result = false;
            var adapter = new ProductAdapter();
            result = adapter.AddProductSubmit(model);

            return Json(result);
        }

        [HttpPost]
        public JsonResult ModifyProductSubmit(ProductModel model)
        {
            var result = false;
            var adapter = new ProductAdapter();
            result = adapter.ModifyProductSubmit(model);

            return Json(result);
        }

        [HttpPost]
        public JsonResult SearchInventory(int? prId)
        {
            var adapter = new ProductAdapter();
            if (prId == null)
            {
                return Json(false);
            }
            else
            {
                inventoryList = adapter.SearchInventory(prId);
                if (inventoryList.Count != 0) { 
                return Json(true);
                }
                else
                {
                    return Json("empty");
                }
            }
        }
    }
}
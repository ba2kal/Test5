﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Adapter;
using WebApplication5.Models.User;

namespace WebApplication5.Controllers
{
    public class UserController : Controller
    {
        public ActionResult RegisterForm()
        {
            return View();
        }

        public ActionResult SignInForm()
        {
            if (Session.Count == 0)
            {
                return View();
            }
            else
            {
                return Redirect("~/Error");
            }
        }

        public JsonResult CheckId(string id)
        {
            bool result = false;
            return Json(result);
        }

        public ActionResult AddUserSubmit(UserModel model)
        {
            var adapter = new UserAdapter();
            var result = adapter.AddUserSubmit(model);

            if (result)
            {
                return Redirect("~/Home");
            }
            return RedirectToAction("RegisterForm");
        }

        [HttpPost]
        public JsonResult SignInSubmit(string id, string pass)
        {
            var adapter = new UserAdapter();
            UserModel result = new UserModel();
            result = adapter.SignInSubmit(id, pass);
            if (result.UserId != null)
            {

                Session["userId"] = id;
                return Json(result);
            }
            else
            {
                return Json("error");
            }
        }

        public ActionResult SignOut()
        {
            Session.Remove("userId");
            return Redirect("~/Home");
        }

        public ActionResult MyPageForm()
        {
            string userId = Session["userId"].ToString();
            UserModel model = new UserModel();
            var adapter = new UserAdapter();
            model = adapter.MyPageForm(userId);

            return View(model);
        }

        [HttpPost]
        public JsonResult CheckUserId(string inputId)
        {
            var result = false;
            var adapter = new UserAdapter();
            result = adapter.CheckUserId(inputId);
            return Json(result);
        }
    }
}
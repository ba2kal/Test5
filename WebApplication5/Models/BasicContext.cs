﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class BasicContext:DbContext
    {
        public DbSet<ProductModel> Products
        {
            get;
            set;
        }

        public DbSet<InventoryModel> Inventory
        {
            get;
            set;
        }
    }
}
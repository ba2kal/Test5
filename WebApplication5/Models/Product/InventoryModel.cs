﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class InventoryModel
    {
        [DisplayName("在庫ID")]
        public int InventoryId
        {
            get;
            set;
        }

        [DisplayName("商品ID")]
        public int ProductId
        {
            get;
            set;
        }

        [DisplayName("商品名")]
        public string InventoryName
        {
            get;
            set;
        }

        [DisplayName("商品価格")]
        public int InventoryPrice
        {
            get;
            set;
        }

        [DisplayName("製造日")]
        public DateTime InventoryCookDate
        {
            get;
            set;
        }

        [DisplayName("賞味期間")]
        public DateTime InventorySafeDate
        {
            get;
            set;
        }

        [DisplayName("賞味期間")]
        public int ProductSafeDay
        {
            get;
            set;
        }

        [DisplayName("数量")]
        public int Amount
        {
            get;
            set;
        }

        [DisplayName("修正日")]
        public DateTime ModifyDate
        {
            get;
            set;
        }
    }
}
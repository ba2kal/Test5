﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class ProductModel
    {
        [DisplayName("商品ID")]
        public int ProductId
        {
            get;
            set;
        }

        [DisplayName("商品名")]
        public string ProductName
        {
            get;
            set;
        }

        [DisplayName("商品価格")]
        public int ProductPrice
        {
            get;
            set;
        }

        [DisplayName("Memo")]
        public string ProductMemo
        {
            get;
            set;
        }

        [DisplayName("賞味期間")]
        public int ProductSafeDay
        {
            get;
            set;
        }

        [DisplayName("登録日")]
        public DateTime RegisterDate
        {
            get;
            set;
        }


        [DisplayName("修正日")]
        public DateTime ModifyDate
        {
            get;
            set;
        }
    }
}
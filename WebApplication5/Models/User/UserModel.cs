﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApplication5.Models.User
{
    public class UserModel
    {
        [DisplayName("会員番号")]
        public int UserNumber
        {
            get;
            set;
        }

        [DisplayName("ID")]
        public string UserId
        {
            get;
            set;
        }

        [DisplayName("PassWord")]
        public string UserPass
        {
            get;
            set;
        }

        [DisplayName("性")]
        public string UserLastName
        {
            get;
            set;
        }

        [DisplayName("名")]
        public string UserFirstName
        {
            get;
            set;
        }

        [DisplayName("性別")]
        public int UserSex
        {
            get;
            set;
        }

        [DisplayName("生年月日")]
        public DateTime UserBirthday
        {
            get;
            set;
        }

        [DisplayName("Email")]
        public string UserEmail
        {
            get;
            set;
        }

        [DisplayName("携帯番号")]
        public string UserPhone
        {
            get;
            set;
        }

        [DisplayName("住所")]
        public string UserAddress
        {
            get;
            set;
        }

        [DisplayName("加入日")]
        public DateTime RegisterDate
        {
            get;
            set;
        }

        [DisplayName("修正日")]
        public DateTime ModifyDate
        {
            get;
            set;
        }
    }
}
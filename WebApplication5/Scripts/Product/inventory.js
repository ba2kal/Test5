﻿
function AddInventorySubmit() {
    var param = $("#add_frm").serializeArray();
    $.ajax({
        url: "/Product/AddProductSubmit",
        type: "post",
        data: param,
        success: function (result) {
            if (result) {
                opener.parent.window.location.reload();
                self.close();
            }
            else {
                alert("error");
            }
        }
    });
}
function ProductFormCancle() {
    window.close();
}

function ModifyProductSubmit() {
    var param = $("#modi_frm").serializeArray();
    $.ajax({
        url: "/Product/ModifyProductSubmit",
        type: "post",
        data: param,
        success: function (result) {
            if (result) {
                opener.parent.window.location.reload();
                self.close();
            }
            else {
                alert("error");
            }
        }
    });
}

function ProductForm(inventoryId, getMode) {
    var invenId = inventoryId;
    var mode = getMode;
    if (mode == 1) {
        window.open("AddInventoryForm", "ProductForm", "width=300, height=450, top=150, left=700");
    }
    else if (mode == 2) {
        window.open("ModifyProductForm?prId=" + prId, "ModifyProductForm", "width=300, height=450, top=150, left=700")
    }
    else if (mode == 3){
        window.open("DetailProductForm?prId=" + prId, "ProductForm", "width=300, height=450, top=150, left=700");
    }
    else if (mode == 4) {
        $.ajax({
            url: "/Product/DeleteProduct",
            type: "POST",
            data: { 'prId': prId},
            success: function (result) {
                if (result == 4) {
                    alert("削除ができました。");
                    location.reload();
                }
                if (result == 0) {
                    alert("error");
                }
            }
        });
    }
    else {
        alert("error");
    }
}

$(document).ready(function () {
    $(".headerfixtable").each(function (i) {
        if ($(this).find("table:eq(1)").height() <= $(this).find(".scroll:eq(0)").height() + 1)
            $(this).find("table:eq(1) col:last").width($(this).find("table:eq(0) col:last").width());
    });
});


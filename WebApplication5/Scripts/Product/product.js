﻿
function AddProductSubmit() {
    var param = $("#add_frm").serializeArray();
    $.ajax({
        url: "/Product/AddProductSubmit",
        type: "post",
        data: param,
        success: function (result) {
            if (result) {
                opener.parent.window.location.reload();
                self.close();
            }
            else {
                alert("error");
            }
        }
    });
}
function ProductFormCancle() {
    window.close();
}

function ModifyProductSubmit() {
    var param = $("#modi_frm").serializeArray();
    $.ajax({
        url: "/Product/ModifyProductSubmit",
        type: "post",
        data: param,
        success: function (result) {
            if (result) {
                opener.parent.window.location.reload();
                self.close();
            }
            else {
                alert("error");
            }
        }
    });
}

$(document).ready(function () {
    $("#searchPr").click(function () {
        $("#panel").slideDown("slow");
    });
});
function ProductForm(productId, getMode) {
    var prId = productId;
    var mode = getMode;
    if (mode == 1) {
        window.open("AddProductForm", "ProductForm", "width=300, height=450, top=150, left=700");
    }
    else if (mode == 2) {
        window.open("ModifyProductForm?prId=" + prId, "ModifyProductForm", "width=300, height=450, top=150, left=700")
    }
    else if (mode == 3){
        window.open("DetailProductForm?prId=" + prId, "ProductForm", "width=300, height=450, top=150, left=700");
    }
    else if (mode == 4) {
        $.ajax({
            url: "/Product/DeleteProduct",
            type: "POST",
            data: { 'prId': prId},
            success: function (result) {
                if (result == 4) {
                    alert("削除ができました。");
                    location.reload();
                }
                if (result == 0) {
                    alert("error");
                }
            }
        });
    }
    else {
        alert("error");
    }
}

function SearchProduct() {
    var getParam = document.getElementById("searchPr").value;
    if (isNaN(getParam)) {
        $(".pd_table").load("/Product/SearchProductTable?mode=string&param=" + getParam);
    }
    if (!isNaN(getParam)) {
        $(".pd_table").load("/Product/SearchProductTable?mode=int&param=" + getParam);
    }
}

function InventoryView(prId) {
    var getParam = prId;
    var viewCheck = $("#inven_view_" + prId).attr("data-view");
    if (viewCheck == "on") {
        $.ajax({
            url: "/Product/SearchInventory",
            type: "POST",
            data: { "prId": getParam },
            success: function (result) {
                if (result) {
                    console.log(result);
                    $("#inven_" + prId).load("/Product/InventoryViewTable?prId="+prId).show("slow");
                    $("#inven_view_" + prId).attr("data-view", "off");
                } else if(result=="empty"){
                    $("#inven_" + prId).load("/Product/InventoryViewTable?prId=0").show("slow");
                    $("#inven_view_" + prId).attr("data-view", "off");
                } else {
                    alert("error!");
                }
            }
        });
    } else if (viewCheck == "off") {
        $("#inven_view_" + prId).attr("data-view", "on");
        $("#inven_" + prId).empty();
    } 
}


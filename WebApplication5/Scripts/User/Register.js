﻿function AddUserSubmit() {
    var check = false;
    check = ValidationCheck();
    if (check) {
        document.getElementById("add_user_frm").submit();
    }
    else {
        alert("error");
    }
}
$(document).ready(function () {
    var date = new Date();
    $("#userBirth").datepicker({
        closeText: '閉じる',
        prevText: '<前',
        nextText: '次>',
        currentText: '今日',
        monthNames: ['1月', '2月', '3月', '4月', '5月', '6月',
            '7月', '8月', '9月', '10月', '11月', '12月'],
        monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月',
            '7月', '8月', '9月', '10月', '11月', '12月'],
        dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
        dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
        dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
        weekHeader: '週',
        dateFormat: 'yy-mm-dd',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        showButtonPanel: true,
        showOtherMonths: true,
        changeYear: true,
        yearSuffix: '年',
        yearRange: "1950:" + date.getFullYear()
    });
});

function CheckUserId() {
    var inputId = document.getElementById("userId").value;
    var length = inputId.trim().length;
 
    if (length == 0) {
        alert("IDを入力して下さい");
    }else{
        $.ajax({
            url: "/User/CheckUserId",
            type: "POST",
            data: { "inputId": inputId },
            success: function (result) {
                if (!result) {
                    alert("このIDは使うことができます。")
                } else {
                    alert("このIDは使うことができません。")
                    document.getElementById("userId").value = "";
                }
            }
        });
    }
}
$(document).ready(function () {
    $("#passMessage").hide();
});

function ValidationCheck() {

}
﻿function SignInSubmit() {
    var id = document.getElementById("userId").value;
    var pass = document.getElementById("userPass").value;
    $.ajax({
        url: "/User/SignInSubmit",
        type: "POST",
        data: { "id": id, "pass": pass },
        success: function (result) {
            if (result.UserId != null) {
                location.href = "/Home";
            } else {
                alert("error");

            }
        }
    });
}